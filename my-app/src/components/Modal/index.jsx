import PropTypes from "prop-types";
import "./modal.scss";

export default function Modal(props) {
  function closeModal(event) {
    if (!event.target.closest(".card") || event.target.closest(".close-btn")) {
      props.closeModal();
    }
  }
  return (
    <div
      className={`${props.modalIsOpen ? "modal-wrap--active" : ""} modal-wrap`}
      onClick={closeModal}
    >
      <div className="card w-50 text-bg-dark mb-3">
        <h5 className="card-header">{props.title}</h5>
        {props.closeButton && (
          <button className="close-btn">
            <span></span>
            <span></span>
          </button>
        )}
        <div className="card-body">
          <p className="card-text text-center">{props.text}</p>
          <div className="main-modal-actions">{props.actions}</div>
        </div>
      </div>
    </div>
  );
}

Modal.propTypes={
    actions: PropTypes.array,
    closeButton: PropTypes.bool,
    modalIsOpen: PropTypes.bool,
    onCloseClick: PropTypes.func,
    text: PropTypes.string,
    title: PropTypes.string,
  }
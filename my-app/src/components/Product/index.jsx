import Button from "../Button";
import PropTypes from "prop-types";
import { useState } from "react";
import "./product.scss";

export default function Product(props) {
  const [modalContent, setModalContent] = useState({
    modalTitle: props.modalTitle,
    modalText: "Click OK to continue",
    modalActions: [
      <Button
        key={1}
        btnStyle={"btn-success"}
        text={"Ok"}
        onButtonClick={() => {
          props.type === "FAV_PRODUCTS"
              ? props.likeItem(props.id)
              : props.toggleCart(props.id);
          props.closeModal();
        }}
      />,
      <Button
        key={2}
        btnStyle={"btn-outline-danger"}
        text={"Cancel"}
        onButtonClick={() => {
          props.closeModal();
        }}
      />,
    ],
    closeButton: true,
  });

  const addedItems = JSON.parse(localStorage.getItem("addedItems"));
  const likedItems = JSON.parse(localStorage.getItem("favoriteItems"));
  return (
    <li
      className="position-relative card me-3 mb-3"
      style={{ maxWidth: 250 + "px" }}
    >
      <div className="card-body">
        <img src={props.imageUrl} alt="" width={"230"} />
        <h5 className="card-title">{props.title}</h5>
        <p className="card-text">Vendor code: {props.code}</p>
        <p className="card-text">Color: {props.color}</p>
        <p className="card-text">Price: {props.price}</p>
        <Button
          btnStyle={"btn-dark"}
          text={props.buttonText}
          onButtonClick={props.handlerButton}
          param={modalContent}
          isDisabled={
            addedItems.find((itemId) => itemId === props.id) &&
            props.type === "ALL_PRODUCTS"
              ? true
              : false
          }
        />
        {props.favIcon ? (
          <img
            src={
              likedItems.find((itemId) => itemId === props.id)
                ? "/images/favorite-active.png"
                : "/images/favorite.png"
            }
            className="favorite-icon"
            width={25}
            height={25}
            alt=""
            onClick={() => {
              props.likeItem(props.id);
            }}
          />
        ) : null}
      </div>
    </li>
  );
}

Product.propTypes = {
  title: PropTypes.string,
  prise: PropTypes.string,
  imageUrl: PropTypes.string,
  id: PropTypes.string,
  color: PropTypes.string,
  code: PropTypes.string,
  likeItem: PropTypes.func,
  handlerButton: PropTypes.func,
  closeModal: PropTypes.func,
  addToCart: PropTypes.func,
  type: PropTypes.string
};

Product.defaultProps={
  type: "ALL_PRODUCTS",
  favIcon: true
}
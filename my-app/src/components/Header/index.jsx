import PropTypes from "prop-types";
import "./header.scss";
import { NavLink } from "react-router-dom";

export default function Header(props) {
  const likedCount = props.likedItems.length;
  const addedCount = props.addedItems.length;
  return (
    <nav className="navbar bg-dark">
      <div className="container-lg">
        <NavLink to={"/"} className="navbar-brand mb-0 h1 text-white">
          IShop
        </NavLink>
        <div className="header__selected-info">
          <NavLink to={"/cart"} className="header__cart">
            <img src="/images/cart.png" alt="" width={"45"} height={"45"} />
            <span className={addedCount ? "selected-counter" : "d-none"}>
              {addedCount}
            </span>
          </NavLink>
          <NavLink to={"/favorite"} className="header__fav">
            <img
              src="/images/favorite-white.png"
              alt=""
              width={"30"}
              height={"30"}
            />
            <span className={likedCount ? "selected-counter" : "d-none"}>
              {likedCount}
            </span>
          </NavLink>
        </div>
      </div>
    </nav>
  );
}

Header.propTypes = {
  addedItems: PropTypes.array,
  likedItems: PropTypes.array,
};

Header.defaultProps = {
  addedItems: 0,
  likedItems: 0,
};

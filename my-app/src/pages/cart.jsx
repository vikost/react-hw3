import PropTypes from "prop-types";
import useFetch from "../hooks/useFetch";
import ProductList from "../components/ProductList";
export function Cart(props) {
  const [products] = useFetch("items.json");
  const addedProducts = products
    ? products.filter((product) => {
        if (props.addedItems.some((itemId) => itemId === product.code)) {
          return product;
        }
      })
    : null;
  return (
    <>
      <div className="container-lg">
        <h1>Cart</h1>
      </div>
      <ProductList
        modalChange={props.modalChange}
        toggleItems={props.toggleCart}
        likeItem={props.toggleFav}
        closeModal={props.closeModal}
        products={addedProducts}
        buttonText="Remove from shopping cart"
        modalTitle="Do you want to remove this item from shopping cart?"
        type={"CART_PRODUCTS"}
        favIcon={false}
      />
    </>
  );
}

Cart.propTypes = {
  addedItems: PropTypes.array,
  toggleCart: PropTypes.func,
  modalChange: PropTypes.func,
  closeModal: PropTypes.func,
};

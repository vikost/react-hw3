import ProductList from "../components/ProductList";
import useFetch from "../hooks/useFetch.js";
export function Home(props) {
  const [products] = useFetch("items.json");

  return (
    <>
      <div className="container-lg">
        <h1>All Products</h1>
      </div>
      <ProductList
        modalChange={props.modalChange}
        toggleItems={props.toggleCart}
        likeItem={props.toggleFav}
        closeModal={props.closeModal}
        products={products}
        buttonText="Buy"
        modalTitle="Do you want to add this item to your shopping cart?"
      />
    </>
  );
}

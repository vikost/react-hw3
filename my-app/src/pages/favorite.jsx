import PropTypes from "prop-types";
import useFetch from "../hooks/useFetch.js";
import ProductList from "../components/ProductList";
export function Favorite(props) {
  const [products] = useFetch("items.json");
  const likedProducts = products
    ? products.filter((product) => {
        if (props.likedItems.some((itemId) => itemId === product.code)) {
          return product;
        }
      })
    : null;
  return (
    <>
      <div className="container-lg">
        <h1>Favorite</h1>
      </div>
      <ProductList
        modalChange={props.modalChange}
        toggleItems={props.toggleCart}
        likeItem={props.toggleFav}
        closeModal={props.closeModal}
        products={likedProducts}
        buttonText="Remove from favorite"
        modalTitle="Do you want to remove this item from favorites?"
        type={"FAV_PRODUCTS"}
        favIcon={false}
      />
    </>
  );
}

Favorite.propTypes = {
  likedItems: PropTypes.array,
  toggleCart: PropTypes.func,
  toggleFav: PropTypes.func,
  modalChange: PropTypes.func,
  closeModal: PropTypes.func,
};

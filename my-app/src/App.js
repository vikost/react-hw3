import ProductList from "./components/ProductList";
import Header from "./components/Header";
import Modal from "./components/Modal";
import { Routes, Route } from "react-router-dom";
import { Home, Cart } from "./pages";
import { useEffect, useState } from "react";
import { Favorite } from "./pages/favorite";

function App() {
  const [likedItems, setLikedItems] = useState(
    JSON.parse(localStorage.getItem("favoriteItems"))
  );
  const [addedItems, setAddedItems] = useState(
    JSON.parse(localStorage.getItem("addedItems"))
  );
  const [modalContent, setModalContent] = useState({
    modalTitle: "",
    modalText: "",
    modalActions: [],
    modalCloseBtn: true,
  });
  const [modalState, setModalState] = useState(false);
  function modalChange({ modalTitle, modalText, modalActions, closeButton }) {
    setModalContent({
      modalTitle: modalTitle,
      modalText: modalText,
      modalActions: modalActions,
      modalCloseBtn: closeButton,
    });
    modalChangeState();
  }
  function modalChangeState() {
    setModalState((prevState) => !prevState);
  }
  function toggleItemtoCart(productId) {
    const updatedItems = JSON.parse(localStorage.getItem("addedItems"));
    const itemIndex = updatedItems.findIndex((item) => item === productId);

    if (itemIndex === -1) {
      updatedItems.push(productId);
    } else {
      updatedItems.splice(itemIndex, 1);
    }
    localStorage.setItem("addedItems", JSON.stringify(updatedItems));
    setAddedItems(updatedItems);
  }
  function toggleItemtoFavorite(productId) {
    const updatedItems = JSON.parse(localStorage.getItem("favoriteItems"));
    const itemIndex = updatedItems.findIndex((item) => item === productId);
    if (itemIndex === -1) {
      updatedItems.push(productId);
    } else {
      updatedItems.splice(itemIndex, 1);
    }
    localStorage.setItem("favoriteItems", JSON.stringify(updatedItems));
    setLikedItems(updatedItems);
  }
  
  return (
    <>
      <Header likedItems={likedItems} addedItems={addedItems} />
      <Routes>
        <Route
          index
          path="/"
          element={
            <Home
              modalChange={modalChange}
              closeModal={modalChangeState}
              toggleCart={toggleItemtoCart}
              toggleFav={toggleItemtoFavorite}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <Cart
              addedItems={addedItems}
              modalChange={modalChange}
              closeModal={modalChangeState}
              toggleCart={toggleItemtoCart}
            />
          }
        />
        <Route
          path="/favorite"
          element={
            <Favorite
              likedItems={likedItems}
              modalChange={modalChange}
              closeModal={modalChangeState}
              toggleFav={toggleItemtoFavorite}
            />
          }
        />
      </Routes>
      <Modal
        title={modalContent.modalTitle}
        text={modalContent.modalText}
        closeButton={modalContent.modalCloseBtn}
        actions={modalContent.modalActions}
        modalIsOpen={modalState}
        closeModal={modalChangeState}
      />
    </>
  );
}

export default App;
